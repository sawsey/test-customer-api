import React from "react";
import { Link } from 'react-router-dom';
import Logo from '../TLH-logo-small.png';


function Homelink(props) {
  return (
    <div className="text-center userful_link mt-5">   
    <div>                   
        <div className="logo_wrap"><img src={Logo} width={250} className ="tlh-logo" alt="logo" /></div>
        <div className="link_wrap">
            <Link to="/userguide" >User Guide</Link>
            <Link to="/apiinfo" >API Info</Link>
            <Link to="/demo" >Demo</Link>
        </div>
        </div>
    </div>
  )
}

export default Homelink