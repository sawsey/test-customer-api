import React, { Component } from "react";
import loadingImg from '../tlh-loading.png';

export class SmsVerification extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userName : "",          
            apikey : "",
            checkParam: "",
            verifyCode : "",
            errorMessage : "",
            successMessage: "",
            clientState: "",  
            loading: false,          
            flag : ""        
        }        
    }    

    getParams = (key) => {
        const params = new URLSearchParams(document.location.search);
        if(params) {
          return params.get(key);
        }
    }

    
    changeuserName = async (e) => {
        this.setState({ userName: e.target.value });
    }

    changeverifyCode = async (e) => {
        this.setState({ verifyCode: e.target.value });
    }
    
    handlephoneSubmit = async (e) => {
        e.preventDefault();
        this.setState({loading: true});
        try {
            var postdata = JSON.stringify({
                mobile: this.state.userName,                
            });
           
            let res = await fetch('https://dev-api.theloginhub.eu/api/v1/sms_login', {
                method: "POST",
                headers: {                   
                    'Content-Type': 'application/json',
                    'apikey' : this.state.apikey,
                },
                body: postdata,
            });
            let resJson = await res.json();
            if (resJson.error === false) {
                if(resJson.data && Array.isArray(resJson.data)) {                    
                    var errorObj = [];
                    resJson.data.forEach(element => {
                        errorObj[element.field] = (errorObj[element.field])? errorObj[element.field] + ', ' + element.message: element.message
                    });
                    this.setState({ errorMessage: errorObj,loading:false});
                } else {
                    this.setState({ errorMessage: resJson.data, loading:false});
                }
            } 
            if (resJson.success === true) {
                this.setState({ successMessage: resJson.data.message ,  errorMessage: "", flag: "true", loading:false});               
            }
            
            
        } catch (err) {
            this.setState({loading:false});
            console.log(err);           

        }
    } 

    handlecodeSubmit = async (e) => {
        this.setState({loading: true});
        e.preventDefault(); 
        try {
            var val = JSON.stringify({
                mobile: this.state.userName,
                code : this.state.verifyCode
            });           
            let res = await fetch('https://dev-api.theloginhub.eu/api/v1/sms_login/activated', {
                method: "POST",
                headers: {                   
                    'Content-Type': 'application/json',                  
                },
                body: val,
            });
            let resData = await res.json();
            var redirectUrl = this.getParams('redirect_url');
            if (resData.error === false) {                                     
                    this.setState({ successMessage: "", errorMessage: resData.data, loading:false});                      
                    if(redirectUrl) {
                        setTimeout(() => {
                            window.location.href = redirectUrl + '?status=false';
                        }, 2000);
                    }              
            } 
            if (resData.success === true) {
                this.setState({ successMessage: resData.data ,  errorMessage: "",  userName: "", loading: false});                
                if(redirectUrl) {
                    setTimeout(() => {
                        window.location.href = redirectUrl + '?status=true&state='+ this.state.clientState;
                      }, 3000);
                }
               
            }
            
            
        } catch (err) {
            this.setState({loading:false});
            console.log(err);           

        }
    } 

    getapikeyfromClientID = async (e) => {
        try { 
            const response = await fetch('https://dev-api.theloginhub.eu/api/v1/companies/info', {
                // mode:  'no-cors',
                method: 'GET',
                headers: {
                'client_id': this.getParams('client_id'),
                'Content-Type': 'application/json'                       
                }
            });
            if (!response.ok) {
                throw new Error('Network response was not ok');
            }
            const data = await response.json();
            if (data.success === true) {
                this.setState({ apikey: data.data.apikey });               
            }
            //console.log(data);
           // setJoke(data[0].joke);
                     
            
        } catch (err) {
            console.log(err);           

        }
        
    }

    componentDidMount () {
        const searchParams = new URLSearchParams(document.location.search);
        if (searchParams && searchParams.has("client_id")) {
            //console.log(searchParams.get("client_id"));
            // const params = {};
            // for (const [key, value] of searchParams.entries()) {
            //     params[key] = value;
            // }  
            // this.setState({ getParems: params });           
            this.setState({checkParam: "queryParams" });            
            //localStorage.setItem("queryParams", JSON.stringify(params));  
            if(searchParams.has("loginHint")) {
                this.setState({ userName:  searchParams.get("loginHint")});  
            }
            if(searchParams.has("state")) {
                this.setState({ clientState:  searchParams.get("state")});  
            }
           this.getapikeyfromClientID();
               
            // window.history.replaceState({}, document.title, window.location.pathname);           
            // window.location.reload();
        }
        // this.getapikeyfromClientID();
        // if (localStorage.getItem("queryParams") !== null) {
        //     const storage = localStorage.getItem("queryParams");
        //     this.setState({checkParam: "queryParams" });
        //     // console.log(storage);
        //     // console.log(JSON.parse(storage).short);
        //     this.setState({userName: JSON.parse(storage).loginHint});
        // }       
        // this.timeoutId = setTimeout(() => {          
        //     localStorage.removeItem('queryParams');           
        //     this.setState({ itemSet: false });
        // }, 10000);  
        
    }
    
    // componentWillUnmount() {
    //     // Clear the timeout to avoid memory leaks
    //     clearTimeout(this.timeoutId);
    //   }


    render() {        
        //console.log(this.state.apikey);

        return (  
                     
            <div className="verification_form">
                {/* <p>Parameters: {this.getParams('id')}</p>  */}  
                {
                    this.state.loading === true ?
                        <div className="loading_wrap">
                        <img src={loadingImg} className ="loading_icon" alt="loading img" /> 
                        <span>Loading..</span>
                    </div>  
                    : null
                }
                              
                <div className="container"> 
                        {
                            this.state.checkParam ? 
                            <div className="row">
                                <div className="col-md-3"></div>
                                <div className="col-md-6">
                                   
                                    <h2 className="text-center pt-5 pb-2">SMS Verification Form</h2>                
                                    {
                                        this.state.successMessage ? <p className='alert alert-success'>{this.state.successMessage}</p>
                                        : (this.state.errorMessage && !Array.isArray(this.state.errorMessage) ) ? <p className='alert alert-warning'>{this.state.errorMessage}</p>
                                        : null
                                    }

                                    {
                                    (this.state.flag === '') ?  
                                    <form onSubmit={this.handlephoneSubmit}>
                                        <div className="form-group"> 
                                            <span className="phone_sign">+47</span>                              
                                            <input type="tel" className="form-control" id="username"  value={this.state.userName} onChange={this.changeuserName} placeholder="Mobile*" />
                                            {this.state.errorMessage && <span className='text text-danger text-small'>{this.state.errorMessage['mobile']}</span>}
                                           
                                                                                     
                                           
                                        </div>
                                        <div className="text-center pt-3">
                                            <button type="submit" className="btn btn-primary login-btn">Login</button>
                                        </div>
                                    </form>  :
                                    <form onSubmit={this.handlecodeSubmit}>
                                        <div className="form-group">                               
                                            <input type="text" className="form-control" id="verifycode"  value={this.state.verifyCode} onChange={this.changeverifyCode} placeholder="Add Code.." />
                                        </div>
                                        <div className="text-center pt-3">
                                            <button type="submit" className="btn btn-primary login-btn">Verify</button>
                                        </div>
                                    </form> 
                                }
                                </div>
                                <div className="col-md-4">
                                    <div className="d-flex mb-3">
                                        <input type="tel" maxLength="1" className="form-control single_field" />
                                        <input type="tel" maxLength="1" className="form-control single_field"/>
                                        <input type="tel" maxLength="1" className="form-control single_field"/>
                                        <input type="tel" maxLength="1" className="form-control single_field"/>                                                
                                    </div>
                                    <button type="submit" className="w-100 btn btn-primary">Verify account</button>
                                </div>
                                <div className="col-md-3"></div>
                            </div>: null
                        }
                                           
                </div>                 
            </div>                           
        )
    }
}