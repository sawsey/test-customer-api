import { jwtDecode } from 'jwt-decode';

// Check if the token is a JWT
export const isJwt = (token) => {
  if (!token) return false;
  const parts = token.split('.');
  console.log(parts);
  if (parts.length !== 3)  return false;
 
  try {
    const header = atob(parts[0]);
    const payload = atob(parts[1]);
   
    return true;
  } catch (error) {
    
    return false;
  }
};

export const isTokenValid = (token) => {
  if (!isJwt(token)) return false;

  try {
    const decodedToken = jwtDecode(token);
    const currentTime = Date.now() / 1000; // in seconds
    return decodedToken.exp > currentTime;
  } catch (error) {
    return false;
  }
};